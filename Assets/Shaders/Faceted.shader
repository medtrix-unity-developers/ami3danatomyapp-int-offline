﻿Shader "Unlit/Faceted"
{
    Properties
    {
        _BaseCol("Base colour", Color) = (1,1,1,1)
        _TopCol("Top colour", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            <a href="/search?Search=%23pragma&Mode=like">#pragma</a> vertex vert
            <a href="/search?Search=%23pragma&Mode=like">#pragma</a> fragment frag
            // make fog work
            <a href="/search?Search=%23pragma&Mode=like">#pragma</a> multi_compile_fog
            
            <a href="/search?Search=%23include&Mode=like">#include</a> "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float3 worldPos: TEXCOORD2;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            fixed4 _TopCol, _BaseCol;
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                float3 x = ddx(i.worldPos);
                float3 y = ddy(i.worldPos);

                float3 norm = -normalize(cross(x,y));

                // Assume basic light shining from above
                float l = saturate(dot(norm, float3(0,1,0)));
                fixed4 col = lerp(_BaseCol, _TopCol, l);

                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}