﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UserInactivityHandler : MonoBehaviour
{
    private bool timerRunning = true;
    private float timePassed = 0;
    public string targetScene = "Loading";
    [Header("Target Time In Minute")]
    public float targetTime = 20f;

    // Start is called before the first frame update
    void Start()
    {
        timePassed = targetTime * 60f;
    }

    // Update is called once per frame
    void Update()
    {
        if (timerRunning)
        {
            timePassed -= Time.deltaTime;
        }

        if (Input.GetMouseButtonDown(0))
        {
            timePassed = targetTime * 60f;
        }

        if (timePassed <= 0 && timerRunning)
        {
            timerRunning = false;
            Debug.Log("TimeIsOver");
            SceneManager.LoadScene(targetScene);
        }
    }
}
