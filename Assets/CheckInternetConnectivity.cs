﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class CheckInternetConnectivity : MonoBehaviour
{
    public GameObject statusOnline, statusOffline;

        void Start()
    {
        InvokeRepeating("CheckInternet", 0f, 1f);
    }

    private void CheckInternet()
    {
        StartCoroutine(CheckInternetConnection(isConnected =>
        {
            if (isConnected)
            {
                // Debug.Log("Internet Available!");
            }
            else
            {
                // Debug.Log("Internet Not Available");
            }
        }));
    }

    IEnumerator CheckInternetConnection(Action<bool> action)
    {
        UnityWebRequest request = new UnityWebRequest("http://google.com");
        yield return request.SendWebRequest();
        if (request.error != null) {
           
            // Debug.Log ("Error");
            action (false);
            statusOnline.SetActive(false);
            statusOffline.SetActive(true);
            //GameObject.FindObjectOfType<LoginHandler>().RB_OnConnectivityLost();
            //Application.OpenURL("http://google.com");    
        } else{
            // Debug.Log ("Success");
            action (true);
            statusOnline.SetActive(true);
            statusOffline.SetActive(false);
        }
    }

    // void Start()
    // {
    //     StartCoroutine(CheckInternetConnection(isConnected =>
    //     {
    //         if (isConnected)
    //         {
    //             Debug.Log("Internet Available!");
    //         }
    //         else
    //         {
    //             Debug.Log("Internet Not Available");
    //         }
    //     }));
    // }

    // IEnumerator CheckInternetConnection(Action<bool> action)
    // {
    //     UnityWebRequest request = new UnityWebRequest("http://google.com");
    //     yield return request.SendWebRequest();
    //     if (request.error != null) {
    //         Debug.Log ("Error");
    //         action (false);
    //     } else{
    //         Debug.Log ("Success");
    //         action (true);
    //     }
    // }
}
