﻿using System;
using UnityEngine;
using TouchScript.Gestures;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class AppManager : MonoBehaviour
{
    public static AppManager Instance;

    [Serializable]
    public class FaceSets
    {
        public List<GameObject> objUT;
        public List<GameObject> objMT;
        public List<GameObject> objLT;
    }

    [Serializable]
    public class FaceLabels
    {
        public List<Label> labelsUT;
        public List<Label> labelsMT;
        public List<Label> labelsLT;
    }

    [Serializable]
    public class Label
    {
        public List<HeadLabel> headLabels = new List<HeadLabel>();
    }

    public FaceSets faceSets = new FaceSets();
    public FaceLabels faceLabels = new FaceLabels();

    int _ActiveSetNo_UT, _ActiveSetNo_MT, _ActiveSetNo_LT;
    public float sliderValue_UT, sliderValue_MT, sliderValue_LT;

    public int _UndoMode;
    public int _TransparencyMode;

    public Slider sliderTransparancy;

    public MeshRenderer meshRenderer_UT, meshRenderer_MT, meshRenderer_LT;

    public Text txtTransparancy;
    public Text txtLayer_UT, txtLayer_MT, txtLayer_LT;
    public Toggle tglLabel;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        for (int i = 0; i < faceSets.objUT.Count; i++)
        {
            faceLabels.labelsUT[i].headLabels.AddRange(faceSets.objUT[i].GetComponentsInChildren<HeadLabel>());

            foreach (var lbl in faceLabels.labelsUT[i].headLabels)
            {
                lbl.gameObject.SetActive(false);
            }
        }

        for (int i = 0; i < faceSets.objMT.Count; i++)
        {
            faceLabels.labelsMT[i].headLabels.AddRange(faceSets.objMT[i].GetComponentsInChildren<HeadLabel>());

            foreach (var lbl in faceLabels.labelsMT[i].headLabels)
            {
                lbl.gameObject.SetActive(false);
            }
        }

        for (int i = 0; i < faceSets.objLT.Count; i++)
        {
            faceLabels.labelsLT[i].headLabels.AddRange(faceSets.objLT[i].GetComponentsInChildren<HeadLabel>());

            foreach (var lbl in faceLabels.labelsLT[i].headLabels)
            {
                lbl.gameObject.SetActive(false);
            }
        }
    }

    public void BtnDissect(int mode)
    {
        switch (mode)
        {
            case 1:
                ChangeLayer(1, _ActiveSetNo_UT, faceSets.objUT, 1);
                break;
            case 2:
                ChangeLayer(2, _ActiveSetNo_MT, faceSets.objMT, 1);
                break;
            case 3:
                ChangeLayer(3, _ActiveSetNo_LT, faceSets.objLT, 1);
                break;
        }
    }

    public void BtnUndo()
    {
        switch (_UndoMode)
        {
            case 1:
                ChangeLayer(1, _ActiveSetNo_UT, faceSets.objUT, -1);
                break;
            case 2:
                ChangeLayer(2, _ActiveSetNo_MT, faceSets.objMT, -1);
                break;
            case 3:
                ChangeLayer(3, _ActiveSetNo_LT, faceSets.objLT, -1);
                break;
        }
    }

    void ChangeLayer(int mode,int ActiveNo, List<GameObject> objSets,int dir)
    {
        switch (mode)
        {
            case 1:
                sliderValue_UT = 0;
                meshRenderer_UT.material.SetFloat("_Transparency", sliderValue_UT);
                break;
            case 2:
                sliderValue_MT = 0;
                meshRenderer_MT.material.SetFloat("_Transparency", sliderValue_MT);
                break;
            case 3:
                sliderValue_LT = 0;
                meshRenderer_LT.material.SetFloat("_Transparency", sliderValue_LT);
                break;
        }

        if (dir == 1)
        {
            ActiveNo++;
            if (ActiveNo >= objSets.Count)
            {
                ActiveNo = objSets.Count - 1;
            }
        }
        else
        {
            ActiveNo--;
            if (ActiveNo < 0)
            {
                ActiveNo = 0;
            }
        }

        for (int i = 0; i < objSets.Count; i++)
        {
            objSets[i].SetActive(false);
        }

        objSets[ActiveNo].SetActive(true);



        switch (mode)
        {
            case 1:
                _ActiveSetNo_UT = ActiveNo;
                //meshRenderer_UT = faceSets.objUT[_ActiveSetNo_UT].GetComponentInChildren<MeshSupport>().GetComponent<MeshRenderer>();
                meshRenderer_UT = faceSets.objUT[_ActiveSetNo_UT].GetComponent<MeshRenderer>();
                break;
            case 2:
                _ActiveSetNo_MT = ActiveNo;
                //meshRenderer_MT = faceSets.objMT[_ActiveSetNo_MT].GetComponentInChildren<MeshSupport>().GetComponent<MeshRenderer>();
                meshRenderer_MT = faceSets.objMT[_ActiveSetNo_MT].GetComponent<MeshRenderer>();
                break;
            case 3:
                _ActiveSetNo_LT = ActiveNo;
                //meshRenderer_LT = faceSets.objLT[_ActiveSetNo_LT].GetComponentInChildren<MeshSupport>().GetComponent<MeshRenderer>();
                meshRenderer_LT = faceSets.objLT[_ActiveSetNo_LT].GetComponent<MeshRenderer>();
                break;
        }



        SetSlider();

        if (tglLabel.isOn)
        {
            EnableCurrentLabels();
        }

        txtLayer_UT.text = "" + (_ActiveSetNo_UT+1);
        txtLayer_MT.text = "" + (_ActiveSetNo_MT+1);
        txtLayer_LT.text = "" + (_ActiveSetNo_LT+1);
    }

    public void SetSlider()
    {
        switch (_TransparencyMode)
        {
            case 1:
                if (_ActiveSetNo_UT == faceSets.objUT.Count - 1)
                {
                    sliderTransparancy.gameObject.SetActive(false);
                }
                else
                {
                    sliderTransparancy.gameObject.SetActive(true);
                }
                sliderTransparancy.value = sliderValue_UT;
                break;
            case 2:
                if (_ActiveSetNo_MT == faceSets.objMT.Count - 1)
                {
                    sliderTransparancy.gameObject.SetActive(false);
                }
                else
                {
                    sliderTransparancy.gameObject.SetActive(true);
                }
                sliderTransparancy.value = sliderValue_MT;
                break;
            case 3:
                if (_ActiveSetNo_LT == faceSets.objLT.Count - 1)
                {
                    sliderTransparancy.gameObject.SetActive(false);
                }
                else
                {
                    sliderTransparancy.gameObject.SetActive(true);
                }
                sliderTransparancy.value = sliderValue_LT;
                break;
        }


    }

    public Material[] materials = null;

    public void OnTransparencySlider()
    {
        txtTransparancy.text = ""+Mathf.RoundToInt(sliderTransparancy.value * 100)+"%";


        switch (_TransparencyMode)
        {
            case 1:
                materials = meshRenderer_UT.materials;
                //meshRenderer_UT.material.SetFloat("_Transparency", sliderTransparancy.value);
                sliderValue_UT = sliderTransparancy.value;
                break;
            case 2:
                materials = meshRenderer_MT.materials;
                //meshRenderer_MT.material.SetFloat("_Transparency", sliderTransparancy.value);
                sliderValue_MT = sliderTransparancy.value;
                break;
            case 3:
                materials = meshRenderer_LT.materials;
                //meshRenderer_LT.material.SetFloat("_Transparency", sliderTransparancy.value);
                sliderValue_LT = sliderTransparancy.value;
                break;
        }

        for(int i=0; i < materials.Length; i++)
        {
            materials[i].SetFloat("_Transparency", sliderTransparancy.value);
        }
    }

    public bool isPaintBrush = false;

    public void DisabledAllLebels()
    {
        for (int i=0;i<faceSets.objUT.Count;i++)
        {
            foreach (var lbl in faceLabels.labelsUT[i].headLabels)
            {
                lbl.gameObject.SetActive(false);
            }
        }

        for (int i = 0; i < faceSets.objMT.Count; i++)
        {
            foreach (var lbl in faceLabels.labelsMT[i].headLabels)
            {
                lbl.gameObject.SetActive(false);
            }
        }

        for (int i = 0; i < faceSets.objLT.Count; i++)
        {
            foreach (var lbl in faceLabels.labelsLT[i].headLabels)
            {
                lbl.gameObject.SetActive(false);
            }
        }
    }

    public void EnableCurrentLabels()
    {
        if (_ActiveSetNo_UT != 0)
        {
            foreach (var lbl in faceLabels.labelsUT[_ActiveSetNo_UT].headLabels)
            {
                lbl.gameObject.SetActive(true);
            }
        }

        if (_ActiveSetNo_MT != 0)
        {
            foreach (var lbl in faceLabels.labelsMT[_ActiveSetNo_MT].headLabels)
            {
                lbl.gameObject.SetActive(true);
            }
        }

        if (_ActiveSetNo_LT != 0)
        {
            foreach (var lbl in faceLabels.labelsLT[_ActiveSetNo_LT].headLabels)
            {
                lbl.gameObject.SetActive(true);
            }
        }
    }

    public void OnLabelToggle(bool isOn)
    {
        if (isOn)
        {
            DisabledAllLebels();
            EnableCurrentLabels();
        }
        else
        {
            DisabledAllLebels();
        }
    }
}