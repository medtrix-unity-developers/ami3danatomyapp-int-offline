﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class ClickableText : MonoBehaviour, IPointerClickHandler
{

    [SerializeField] private GameObject privacyPopup;
    [SerializeField] private GameObject termsPopup;

    void Start()
    {
        Debug.Log(this.gameObject.name);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        var text = GetComponent<TextMeshProUGUI>();
        if(eventData.button == PointerEventData.InputButton.Left)
        {
            int linkIndex = TMP_TextUtilities.FindIntersectingLink(text, Input.mousePosition, null);
            if(linkIndex > -1)
            {
                var linkInfo = text.textInfo.linkInfo[linkIndex];
                var linkId = linkInfo.GetLinkID();
                //var itemData = FindObjectOfType<ItemDataController>().Get(linkId);
                //Debug.Log("CLICKED LINK NAME : " + linkInfo.GetLinkID());

                if (linkInfo.GetLinkID() == "Privacy")
                {
                    privacyPopup.gameObject.SetActive(true);
                    termsPopup.gameObject.SetActive(false);
                }
                else if(linkInfo.GetLinkID() == "Terms")
                {
                    privacyPopup.gameObject.SetActive(false);
                    termsPopup.gameObject.SetActive(true);
                }

            }
        }
    }
 
}
