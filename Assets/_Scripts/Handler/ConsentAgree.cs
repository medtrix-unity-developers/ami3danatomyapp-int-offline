﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConsentAgree : MonoBehaviour
{
    [SerializeField] private Button LoginButton;
    [SerializeField] private Toggle toggle;
    [SerializeField] private Sprite loginBtnActive;
    [SerializeField] private Sprite loginBtnInActive;

    public void OnAgreeSelected()
    {
        if(toggle.isOn)
        {
            LoginButton.interactable = true;
            LoginButton.GetComponent<Image>().sprite = loginBtnActive;
         
        }
        else
        {
            LoginButton.interactable = false;
            //LoginButton.interactable = true; // Has to remove this line and enable the top line.
            LoginButton.GetComponent<Image>().sprite = loginBtnInActive;
        }

    }

     

}
