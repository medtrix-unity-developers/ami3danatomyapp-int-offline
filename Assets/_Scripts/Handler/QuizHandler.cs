﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public enum QType
{
    OPTION,
    DRAG_DROP,
    LOCATE,
}

[System.Serializable]
public class Options
{
    public string language;
    public List<string> strOption = new List<string>();
}

[System.Serializable]
public class Questions
{
    public string language;
    public string strQuestion;
    public string strQueInfo;
}

public class QuizHandler : MonoBehaviour
{
    public static QuizHandler Instance;

    [System.Serializable]
    public class QuizData
    {
        public int qNo;
        public QType qType;
        public Sprite quizImage;
        //public string question;
        public List<Questions> questions = new List<Questions>();
        public List<Options> options = new List<Options>();
        //public List<string> strOption = new List<string>();        
        public List<Transform> posOption = new List<Transform>();
        //public string correctAns;
        public List<Questions> correctAnswer = new List<Questions>();
        public int correctIndex;
    }

    public int qNo;

    public Text txtQuiz, txtIndex;
    public TextMeshProUGUI txtScoreGO, txtInfo;
    public Image imgQuiz;
    public List<QuizData> quizData = new List<QuizData>();
    public List<DropTarget> dragTarget = new List<DropTarget>();
    public List<GameObject> dragStart = new List<GameObject>();
    public List<QuizOptionCell> objOptions = new List<QuizOptionCell>();
    public GameObject prfbOption, prfbDrager;
    public GameObject panel_QuizDone; 
    public Button btnSubmit;

    public int languageIndex;

    void Awake()
    {
        Instance = this;

        switch (PlayerPrefs.GetString("CurrentLanguge", "EN"))
        {
            case "EN":
                languageIndex = 0;
                break;
            case "CH":
                languageIndex = 1;
                break;
            case "JP":
                languageIndex = 2;
                break;
        }
    }   

    void Start()
    {
        txtScore.text = LocalisationHandler.Instance.GetScreenText("score") + " : " + score;
        Invoke("Init", 0.35f);
    }

    public void Init()
    {
        score = 0;
        //ShuffleQuiz(quizData);
        qNo = 0;

        Debug.Log(quizData[qNo].options[languageIndex].strOption[0]);
        SetQuestion();
    }

    IEnumerator SetQuestionCo(float time)
    {
        time = 7f;
        yield return new WaitForSeconds(time);
        txtInfo.gameObject.SetActive(false);
        qNo++;
        if (qNo >= quizData.Count)
        {
            Debug.Log("Completed");
            txtScoreGO.text = "Score : " + score;
            panel_QuizDone.SetActive(true);
            yield break;
        }

        SetQuestion();
    }

    public void BtnRestart()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Quiz");
    }

    public void SetQuestion()
    {
        foreach (var obj in objOptions)
        {
            Destroy(obj.gameObject);
        }

        objOptions.Clear();

        for (int i = 0; i < dragTarget.Count; i++)
        {
            dragTarget[i].gameObject.SetActive(false);
            dragStart[i].SetActive(false);
        }

        txtIndex.text = "" + (qNo + 1) + ".";
        txtQuiz.text =  quizData[qNo].questions[languageIndex].strQuestion;
        txtInfo.text = quizData[qNo].questions[languageIndex].strQueInfo;
        imgQuiz.sprite = quizData[qNo].quizImage;  

        GameObject objOption = null;
        GameObject prfb = null;

        switch (quizData[qNo].qType)
        {
            case QType.OPTION:
                btnSubmit.gameObject.SetActive(false);
                prfb = prfbOption;
                break;

            case QType.DRAG_DROP:
                for (int i = 0; i < quizData[qNo].options[languageIndex].strOption.Count; i++)
                {
                    dragTarget[i].gameObject.SetActive(true);
                    dragTarget[i].Init(quizData[qNo].posOption[i], quizData[qNo].options[languageIndex].strOption[i]);
                }
                btnSubmit.gameObject.SetActive(true);
                prfb = prfbDrager;
                SuffleList(quizData[qNo].options[languageIndex].strOption);
                break;

            case QType.LOCATE:
                for (int i = 0; i < quizData[qNo].options[languageIndex].strOption.Count; i++)
                {
                    dragTarget[i].gameObject.SetActive(true);
                    dragTarget[i].Init(quizData[qNo].posOption[i], quizData[qNo].options[languageIndex].strOption[i]);
                }

                btnSubmit.gameObject.SetActive(false);
                prfb = prfbOption;
                SuffleList(quizData[qNo].options[languageIndex].strOption);
                break;
        }

        for (int i = 0; i < quizData[qNo].options[languageIndex].strOption.Count; i++)
        {
            if(quizData[qNo].qType != QType.LOCATE)
                dragStart[i].SetActive(true);

            objOption = Instantiate(prfb);
            objOption.transform.SetParent(quizData[qNo].qType == QType.LOCATE ? dragTarget[i].transform : dragStart[i].transform, false);

            string tempCorrectAns = quizData[qNo].correctAnswer.Count == 0 ? "" : quizData[qNo].correctAnswer[languageIndex].strQuestion;
            objOption.GetComponent<QuizOptionCell>().Init(i,quizData[qNo].options[languageIndex].strOption[i], tempCorrectAns , quizData[qNo].correctIndex, quizData[qNo].qType);
            objOptions.Add(objOption.GetComponent<QuizOptionCell>());
        }
    }

    void SuffleList(List<string> pos)
    {
        for (int i = 0; i < pos.Count; i++)
        {
            string temp = pos[i];
            int randomIndex = Random.Range(i, pos.Count);
            pos[i] = pos[randomIndex];
            pos[randomIndex] = temp;
        }
    }

    public void ActivateSubmitButton()
    {
        bool status = true;

        for (int i = 0; i < quizData[qNo].options[languageIndex].strOption.Count; i++)
        {
            if (dragStart[i].transform.childCount > 0)
            {
                status = false;
            }
        }

        btnSubmit.interactable = status;
    }

    public void BtnSubmit()
    {
        txtInfo.gameObject.SetActive(true);
        int falseCount = 0;

        for (int i = 0; i < quizData[qNo].options[languageIndex].strOption.Count; i++)
        {
            if (quizData[qNo].qType == QType.DRAG_DROP)
            {
                bool temp = objOptions[i].CheckAnswerDrag();

                if (!temp)
                    falseCount++;
            }
            else
            {
                objOptions[i].CheckAnswerOption();
            }
        }

        if (quizData[qNo].qType == QType.DRAG_DROP)
        {
            StartCoroutine(SetQuestionCo(5));

            if (falseCount == 0)
                UpdateScore();
        }
        else
        {
            StartCoroutine(SetQuestionCo(1));
        }
    }

    void ShuffleQuiz(List<QuizData> quiz)
    {
        for (int i = 0; i < quiz.Count; i++)
        {
            QuizData temp = quiz[i];
            int randomIndex = Random.Range(i, quiz.Count);
            quiz[i] = quiz[randomIndex];
            quiz[randomIndex] = temp;
        }
    }

    public int score = 0;
    public Text txtScore;

    public void UpdateScore()
    {
        score = score + 5;
        txtScore.text = LocalisationHandler.Instance.GetScreenText("score") + " : " + score;
    }

    public void DisableOption()
    {
        for (int i=0; i<objOptions.Count;i++)
        {
            objOptions[i].GetComponent<Button>().interactable = false;
        }
    }
}