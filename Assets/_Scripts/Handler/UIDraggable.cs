﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIDraggable: MonoBehaviour,IBeginDragHandler,IDragHandler,IEndDragHandler
{
    Vector2 startPos;

    public void OnDrag(PointerEventData eventData)
    {
        this.transform.position += (Vector3)eventData.delta;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        //throw new System.NotImplementedException();
        startPos = this.transform.position;
        AppManager.Instance.isPaintBrush = true;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        this.transform.position = startPos;
        AppManager.Instance.isPaintBrush = false;
        //throw new System.NotImplementedException();
    }
}