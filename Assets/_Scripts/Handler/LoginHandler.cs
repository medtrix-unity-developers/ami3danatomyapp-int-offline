﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Boomlagoon.JSON;

public class LoginHandler : MonoBehaviour
{
    public Text txtError;
    public InputField inptUsername, inptPassword;
    public Text txtWelcome;
    public GameObject Panel_Login, PanelWelcome, PanelConsent_FirstTime, PanelConsent_Again, PanelConsent_Privacy, PanelConsent_Terms;
    public Dropdown dropdown;
    public Button CountrySubmitButton;
    public Sprite[] SubmitBtns;

    public string endpointRegion = "";
    public List<string> EU_Countries = new List<string>() {
        "Austria",
        "Bahrain",
        "Belgium",
        "Bulgaria",
        "Czech Republic",
        "Denmark",
        "Egypt",
        "Estonia",
        "Finland",
        "France",
        "Germany",
        "Greece",
        "Hungary",
        "Israel",
        "Italy",
        "Jordan",
        "Kuwait",
        "Latvia",
        "Lebanon",
        "Lithunia",
        "Netherlands",
        "Norway",
        "Oman",
        "Poland",
        "Qatar",
        "Romania",
        "Russia",
        "Saudi Arabia",
        "Serbia",
        "Slovakia",
        "South Africa",
        "Spain",
        "Sweden",
        "Switzerland",
        "Turkey",
        "Ukraine",
        "United Arab Emirates",
        "United Kingdom" };

    public List<string> LACAN_Countries = new List<string>() {
        "Argentina",
        "Brazil",
        "Canada",
        "Chile",
        "Columbia",
        "Mexico"};

    public List<string> APAC_Countries = new List<string>() {
        "Australia",
        "New Zealand" };


    // OAuth2 flow variables.
    private string requestedURL;
    UniWebView webView;
    private GameObject webViewGameObject;
    [SerializeField]private GameObject customWebToolbar;
    [SerializeField] private RectTransform browserRectTransform;

    [Header("Authentication Settings (keep those secret!)")]
    [Tooltip("Change default value when using a sandbox or restricting authentication domain")]
    public List<string> oAuthEndpoint;
    public string oAuthEndpointDynamic;


    [Tooltip("Get this value from your Salesforce connected application")]
    public List<string> consumerKey; //  = "3MVG96mGXeuuwTZibJF8Ab1857dAuUafEflpSIcTh4ELOtv54blaxCsOwiSnueSEAdxTzFINg4FzOqiUVWvx8";
    [Tooltip("Get this value from your Salesforce connected application")]
    public List<string> consumerSecret; // = "9D1FB456365DE716A8080786AA3CEF4C1079200CCD7723F8A7E2D0D23764387E";


    void Start()
    {
        //PlayerPrefs.DeleteAll();
        //if (PlayerPrefs.GetInt("IsUserLoggedAlready") == 1)
        //{
        //    PanelConsent_Again.SetActive(true);
        //    PanelConsent_FirstTime.SetActive(false);
        //}
        //else
        //{
        //    PanelConsent_FirstTime.SetActive(true);
        //    PanelConsent_Again.SetActive(false);
        //}

        //PlayerPrefs.SetInt("IsUserLoggedAlready", 1);

        //consumerKey = new List<string>(){
        //    "3MVG92H4TjwUcLlKA_Ay0cvzBFVPYKuAA9IUjfi0HnnY8t77SlNdbixNM7rjg6o2xPl2QcUvrfxlV.F65FphQ",
        //    "3MVG91LYYD8O4krQrIapNYSSx2pid70J3mYa3CMC2rYD46RTphzOr.OiqNK5Lg3ETOVMPgHXlWSss357FwzI9",
        //    "3MVG9ZL0ppGP5UrAAEtitUJf16VBQUbk1D8_63EtadFVVq.N_ZaNqKnsGUTSR4UdmrhTorg4xGJ24Q55yn_wZ"
        //};
        //consumerSecret = new List<string>()
        // {
        //    "C6111D13A85893F310B6AF995AC67A3644B0F1C8F47D10A36F9110E8436B8080",
        //    "28BC403CE647803929A370E246916A89130DCE21647B4A9C45D56DED9C846567",
        //    "77C4C0D28195F8CADDD5E87296895DC2B51D769E0B269A1F65DDC21A7A48D33F"
        // };
        //// SIT End points..
        //oAuthEndpoint = new List<string>(){
        //    "http://eupc-allergancommunityeu.cs82.force.com/AMIAnatomyApp/services/oauth2/authorize?response_type=token&client_id=3MVG92H4TjwUcLlKA_Ay0cvzBFVPYKuAA9IUjfi0HnnY8t77SlNdbixNM7rjg6o2xPl2QcUvrfxlV.F65FphQ&redirect_uri=sfdc://mobileapp/done&state=mystate",
        //    "http://latampc-allergancommunitylacan.cs110.force.com/AMIAnatomyApp/services/oauth2/authorize?response_type=token&client_id=3MVG91LYYD8O4krQrIapNYSSx2pid70J3mYa3CMC2rYD46RTphzOr.OiqNK5Lg3ETOVMPgHXlWSss357FwzI9&redirect_uri=sfdc://mobileapp/done&state=mystate",
        //    "https://allergancommunityapac.force.com/AMIAnatomyApp/services/oauth2/authorize?response_type=token&client_id=3MVG9ZL0ppGP5UrAAEtitUJf16VBQUbk1D8_63EtadFVVq.N_ZaNqKnsGUTSR4UdmrhTorg4xGJ24Q55yn_wZ&redirect_uri=sfdc://mobileapp/done&state=mystate"
        //};

        if (NavDrawerHandler.Instance)
        {
            NavDrawerHandler.Instance.Hide();
        }

        //OnWebViewInit();
    }

    private void Update()
    {
        if (inptUsername.text != "" && inptPassword.text != "")
        {
            CountrySubmitButton.interactable = true;
        }
        else
        {
            CountrySubmitButton.interactable = false;
        }
    }

    public void OnWebViewInit()
    {
        webViewGameObject = new GameObject("UniWebView", typeof(RectTransform));
        webView = webViewGameObject.AddComponent<UniWebView>();
        webViewGameObject.transform.SetParent(browserRectTransform);

        //webView.Frame = new Rect(230f, (Screen.height / 2) - 50f, Screen.width - 460f , (Screen.height/2) + 130f);
        webView.ReferenceRectTransform = browserRectTransform;
        webView.SetShowSpinnerWhileLoading(true);

        webView.OnShouldClose += (view) => {
            Debug.Log(" On Should Close...");
            GameObject prefabs = GameObject.Find("UniWebView");
            Destroy(prefabs);
            customWebToolbar.SetActive(false);
            browserRectTransform.gameObject.SetActive(false);
            return true;
        };

        webView.AddUrlScheme("sfdc");
        webView.OnMessageReceived += (view, message) => {
            //print(message.RawMessage);
            Debug.Log(" OnMessageReceived " + message.Path);

            //JSONObject obj = JSONObject.Parse(message.Path);
            //string token = obj.GetString("access_token");
            //string instanceUrl = obj.GetString("instance_url");
            if (message.Path.Contains("access_token"))
            {
                CloseWebBrowserOnAllowed();
            }
            else
            {
                CloseWeBrowserOnDenied();
            }
        };
    }

    void DestroyGameObjectInScene(string objName)
    {
        GameObject[] prefabs = GameObject.FindGameObjectsWithTag("UniWebView");
        for (var i = 0; i < prefabs.Length; i++)
        {
            Destroy(prefabs[i]);
        }
    }


    public void BtnLogin()
    {
        if (inptUsername.text.ToLower().Equals("ami_essential") && inptPassword.text.Equals("3Danatomy"))
        {
            PlayerPrefs.SetInt("IsUserLoggedAlready", 1);
            //GUIManager.Instance.ScreenController(ScreenName.Panel_Welcome);
            PanelWelcome.SetActive(true);
            Panel_Login.SetActive(false);
        }
        else
        {
            //txtError.text = "The username or password you have entered is incorrect. Please try again";
            txtError.gameObject.SetActive(true);
            Invoke("DisableError", 3);
        }

        //PanelWelcome.SetActive(true);
        //Panel_Login.SetActive(false);

        //if (webViewGameObject == null)
        //    OnWebViewInit();

        //StartCoroutine(SendAuthRequest());
    }


    IEnumerator SendAuthRequest()
    {
        WWWForm form = new WWWForm();
        //form.AddField("username", username);
        //form.AddField("password", password);

        if(endpointRegion == "eu")
        {
            oAuthEndpointDynamic = oAuthEndpoint[0];
            form.AddField("client_secret", consumerSecret[0]);
            form.AddField("client_id", consumerKey[0]);
            form.AddField("grant_type", "Code");
            form.AddField("response_type", "token");
        }
        else if (endpointRegion == "lacan")
        {
            oAuthEndpointDynamic = oAuthEndpoint[1];
            form.AddField("client_secret", consumerSecret[1]);
            form.AddField("client_id", consumerKey[1]);
            form.AddField("grant_type", "Code");
            form.AddField("response_type", "token");
        }
        else if (endpointRegion == "apac")
        {
            oAuthEndpointDynamic = oAuthEndpoint[2];
            form.AddField("client_secret", consumerSecret[2]);
            form.AddField("client_id", consumerKey[2]);
            form.AddField("grant_type", "Code");
            form.AddField("response_type", "token");
        }

        // Send request & parse responseOnDeepLinkURLOpened
        UnityWebRequest request = UnityWebRequest.Post(oAuthEndpointDynamic, form);

        request.timeout = 15;
        request.useHttpContinue = false;
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            //yield break;
            logResponseError(request);
            requestedURL = request.url;
            //controller.OnSubmitClicked(request.url);
        }
        else
        {
            logResponseSuccess(request);
            Debug.Log("HTTP SUCCESS SIVA:");
        }
        yield return true;
        OpenWEBBrowser(requestedURL);
    }

    public void OpenWEBBrowser(string requestedURL)
    {
        //Debug.Log("OPEN WEB BroWSER" + requestedURL);
        if (string.IsNullOrEmpty(requestedURL)) { return; }
        webView.Load(requestedURL);
        webView.Show();
        customWebToolbar.SetActive(true);
        browserRectTransform.gameObject.SetActive(true);
    }

    private void CloseWebBrowserOnAllowed()
    {
        Debug.Log(" On CloseWebBrowserOnAllowed...");
        PlayerPrefs.SetInt("IsUserLoggedAlready", 1);
        webView.InternalOnShouldClose();
        PanelWelcome.SetActive(true);
        Panel_Login.SetActive(false);

        customWebToolbar.SetActive(false);
        browserRectTransform.gameObject.SetActive(false);
    }

    private void CloseWeBrowserOnDenied()
    {
        webView.InternalOnShouldClose();
        txtError.text = "Access Denied";
        txtError.gameObject.SetActive(true);
        customWebToolbar.SetActive(false);
        browserRectTransform.gameObject.SetActive(false);

    }

    private void logResponseSuccess(UnityWebRequest request)
    {
        Debug.Log("Salesforce HTTP request:> " + request.method + " RESPONSE CODE:::> " + request.responseCode + " URL ::::> " + request.url);
        Debug.Log("REQUEST DOWNLOAD HANDLER :>" + request.downloadHandler.text);
    }

    private void logResponseError(UnityWebRequest request)
    {
        Debug.LogError("Salesforce HTTP request: " + request.method + " " + request.responseCode + " " + request.url);
        Debug.LogError(request.error);
        Debug.LogError(request.downloadHandler.text);
    }

    public void OnConcentContinue(string concentType)
    {
        Panel_Login.SetActive(true);

        if (concentType == "First")
        {
            PanelConsent_FirstTime.SetActive(false);
        }
        else
        {
            PanelConsent_Again.SetActive(false);
        }
    }

    public void BtnWelcome()
    {     
        UnityEngine.SceneManagement.SceneManager.LoadScene("Label");
    }

    void DisableError()
    {
        txtError.gameObject.SetActive(false);//.text = "";
    }

    public void OnDropDownValueChanged()
    {
        Debug.Log("COUNTRY NAME :"+ dropdown.options[dropdown.value].text) ;
        string selectedCountry = dropdown.options[dropdown.value].text;
        endpointRegion = "";

        if(selectedCountry != "-Select Country-")
        {
            CountrySubmitButton.GetComponent<Image>().sprite = SubmitBtns[1];
            CountrySubmitButton.interactable = true;
        }
        else
        {
            CountrySubmitButton.GetComponent<Image>().sprite = SubmitBtns[0];
            CountrySubmitButton.interactable = false;
        }


        if (EU_Countries.Contains(selectedCountry))
        {
            //Debug.Log("COUNTRIES LIST in EU REGION :");
            endpointRegion = "eu";
        }
        else if (LACAN_Countries.Contains(selectedCountry))
        {
            //Debug.Log("COUNTRIES LIST in LACAN REGION :");
            endpointRegion = "lacan";

        }
        else if (APAC_Countries.Contains(selectedCountry))
        {
            //Debug.Log("COUNTRIES LIST in APAC REGION :");
            endpointRegion = "apac";

        }
    }

    public void OnPrivacyTermsClosed()
    {
        PanelConsent_Privacy.SetActive(false);
        PanelConsent_Terms.SetActive(false);
    }

    public void OnForwardNavigate()
    {
        webView.GoForward();
        Debug.Log("OnForwardNavigate clicked...");
    }
    public void OnBackNavigate()
    {
        webView.GoBack();
        Debug.Log("OnBackNavigate clicked...");

    }

    public void OnWebViewDone()
    {
        webView.InternalOnShouldClose();
        Debug.Log("OnWebViewDone done...");
    }

    //public void CopyToClipboard()
    //{
    //    GUIUtility.systemCopyBuffer = "Copied Rishabh Bhushan";
    //}

    public void OnEmailClicked()
    {
        GUIUtility.systemCopyBuffer = "go-medical-aesthetics@allergan.com";
        string email = "go-medical-aesthetics@allergan.com";
        string subject = MyEscapeURL("--Subject-- Allergan Aesthetics");
        string body = MyEscapeURL("--BODY--\r\nFor Technical Support.");
        Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
    }

    string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }

}
 