﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetText : MonoBehaviour
{
    public string key;


    Text txt;
    void Start()
    {
        txt = this.GetComponent<Text>();

        switch (PlayerPrefs.GetString("CurrentLanguge", "EN"))
        {
            case "EN":
                txt.font = LocalisationHandler.Instance.fontEN;
                break;
            case "CH":
                txt.font = LocalisationHandler.Instance.fontCH;
                break;
            case "JP":
                txt.font = LocalisationHandler.Instance.fontJP;
                break;
        }

        if (!string.IsNullOrEmpty(key))
            txt.text = LocalisationHandler.Instance.GetScreenText(key);
    }
}
