﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DropTarget : MonoBehaviour
{
    public Transform rectQuiz;
    public Transform from;
    public RectTransform rectLine;

    public string strOption;

    public void Init(Transform to, string option)
    {
        strOption = option;
        Vector2 rot = to.localPosition - rectQuiz.InverseTransformPoint((from.position));
        rectLine.localEulerAngles = new Vector3(0, 0, FindDegree(rot.y, rot.x));

        rectLine.transform.localScale = new Vector3(Vector3.Distance(to.position, from.position) * 0.1f, 0.5f, 1);
        //rectLine.transform.localScale = new Vector3(Vector3.Distance(rectQuiz.TransformPoint(to), from.position) * 0.1f, 0.5f, 1);
    }

    public float FindDegree(float x, float y)
    {
        float value = (float)((Mathf.Atan2(x, y) / Mathf.PI) * 180f);

        if (value < 0) value += 360f;

        return value;
    }
}