﻿
public class Constant
{

}

public enum ScreenName
{
    Panel_BG,
    Panel_NavDrawer,
    Panel_Login,
    Panel_Welcome,
    Panel_GamePlay,
    Panel_Quiz,
    Panel_Video,
    Panel_VideoPlayer,
    Panel_LabelAdditional,
    Panel_Reference,
    Panel_MDCode,
}

public enum LabelsKey
{
    None,
    //set_02
    Label_Central_forehead_fat_pad,
    Label_Medial_forehead_fat_pad,
    Label_Supratrochlear_artery,
    Label_Superficial_fat_compartments,
    Label_Lateral_temporal_cheek_fat_pad,
    Label_Temporal_crest,
    Label_Pre_platysmal_fat,
    Label_Jowl_fat_pads_upper,
    Label_Jowl_fat_pads_lower,
    Label_Superior_Jowl_fat_pad,

    //set_03
    Label_Fibres_of_frontalis,
    Label_Frontal_branch_of_superficial_temporal_artery,
    Label_Supraorbital_artery,
    Label_Superficial_temporal_fascia_SMAS,
    Label_Parietal_branch_of_superficial_temporal_artery,
    Label_Alaque_nasi_muscle,
    Label_Levator_labii_superioris_alaeque_muscle,
    Label_Zygomaticus_major_muscle,
    Label_Zygomaticus_minor_muscle,
    Label_Fibres_of_orbicularis_oculi,
    Label_Depressor_Labii_inferioris,
    Label_Modiolus,
    Label_Depressor_Anguli_oris,
    Label_Mentalis_muscle,
    Label_Risorius_muscle,
    Label_Inferior_jowl_fat_pad,

    //set_04
    Label_Facial_vein,
    Label_Facial_artery,
    Label_Superior_labial_artery,
    Label_Suborbicularis_oculi_fat,
    Label_Dorsal_nasal_artery,
    Label_Inferior_alar_artery,
    Label_Inferior_labial_artery,

    //set_05
    Label_Retro_orbicularis_oculus_fat,
    Label_Galea_aponeurotica,
    Label_Superficial_temporal_fascia,
    Label_Superficial_layer_of_deep_temporal_fascia,
    Label_SMAS_layer_extending_superiorly_inferiorly_laterally_and_medially,
    Label_Parotido_massetric_fascia,

    //set_06
    Label_Deep_layer_of_deep_temporal_fascia,
    Label_SMAS_layer_with_all_the_muscle_attachments,
    Label_Superficial_temporal_artery,

    //set_07
    Label_Exposed_temporalis_muscle,
    Label_Temporalis,
    Label_Deep_temporal_vessels,
    Label_Origin_of_Corrugator_supercilii,
    Label_Masseter_muscle,
    Label_Maxilla,

    //set_08
    Label_Frontal_bone,
    Label_Temporal_bone,
    Label_Temporal_fossa,
    Label_Temporalis_muscle,
    Label_Extension_of_buccal_fat_into_temporal_fossa,
    Label_Zygomatic_arch_of_the_maxilla,
    Label_Mandible,

    //set_09
    Label_Supraorbital_foramen,
    Label_Zygomaticofacial_foramen,
    Label_Infraorbital_foramen,
    Label_Extension_of_buccal_fat_below_zygomatic_arch,
    Label_Mental_foramen,
    Label_Lateral_temporal_fat_pad,

    Label_Nasolabial_superficial_fat_compartments,
    Label_Medial_cheek_fat_pad,
    Label_Jowl_fat_pads,
    Label_Corrugator_supercilii_muscle,
    Label_Periosteum,
    Label_Zygomatic_arch,
    Label_Nasal_bone,
    Label_Temporal_extension_of_buccal_fat,
    Label_Buccal_fat_pad,
}

[System.Serializable]
public class LabelInfo
{
    public LabelsKey labelsKey;
    public string strSearch;
}