﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasScaller : MonoBehaviour
{
    public CanvasScaler canvasScaler;
    
    void Awake()
    {
        canvasScaler = this.GetComponent<CanvasScaler>();
        canvasScaler.referenceResolution = new Vector2(Screen.width / 2, Screen.height / 2);
    }
}
