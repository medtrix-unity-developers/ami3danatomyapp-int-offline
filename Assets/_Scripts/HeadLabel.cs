﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadLabel : MonoBehaviour
{
    LineRenderer lineRenderer;
    GameObject labelObject;

    public TextMesh labelTextMesh;
    public Transform lineFrom;
    public Transform lineTo;

    Transform textParent;

    public Vector3 linePadding;

    public float hideAngle = 45f;

    [Header("Debug")]
    public float angleBetween;

    public LabelInfo labelInfo;
    GameObject objInfo;

    private void Awake()
    {
        labelInfo.labelsKey = (LabelsKey)Enum.Parse(typeof(LabelsKey), this.gameObject.name);
    }

    private void Start()
    {
        textParent = labelTextMesh.transform.parent.transform;
        SetupLabel();

        if (!PlayerPrefs.GetString("CurrentLanguge").Equals("EN"))
        {
            labelTextMesh.text = LabelsHandler.Instance.GetTitle("" + labelInfo.labelsKey);
        }
    }

    void SetupLabel()
    {
        labelObject = Instantiate(Resources.Load<GameObject>("LabelPrefab"));

        labelObject.transform.SetParent(textParent.transform, false);
        lineRenderer = labelObject.GetComponent<LineRenderer>();
        lineRenderer.SetPositions(new Vector3[]
        {
            lineFrom.transform.localPosition,
            textParent.InverseTransformPoint(lineTo.transform.position)
        });

        if (labelInfo.labelsKey != LabelsKey.None)
        {
            objInfo = Instantiate(Resources.Load<GameObject>("prfbInfo"));
            objInfo.GetComponent<CellLabelDescription>().OnLabelInfo += OnLabelInfo;
            objInfo.transform.SetParent(textParent, false);
            //objInfo.transform.localEulerAngles = labelTextMesh.transform.parent.transform.localEulerAngles;
            objInfo.transform.localPosition = new Vector3(-transform.GetComponentInChildren<BoxCollider>().transform.localScale.x/2 - 0.1f,0,0);
        }
    }

    private void Update()
    {
        angleBetween = GetAngleFromCamera();

        if (Mathf.Abs(angleBetween) >= hideAngle)
        {
            textParent.gameObject.SetActive(false);
            if (objInfo != null)
                objInfo.SetActive(false);
        }
        else
        {
            textParent.gameObject.SetActive(true);
            if (objInfo != null)
                objInfo.SetActive(true);
        }
    }

    public float GetAngleFromCamera()
    {
        Vector3 cameraMainPos = Camera.main.transform.position;
        Vector3 dir = cameraMainPos - textParent.position;
        return Vector3.Angle(-labelTextMesh.transform.forward, new Vector3(dir.x, 0f, dir.z));
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;

        if (lineFrom != null && lineTo != null)
            Gizmos.DrawLine(lineFrom.position, lineTo.position);

        Gizmos.color = Color.green;

        if (lineTo != null)
            Gizmos.DrawSphere(lineTo.transform.position, 0.003f);
    }

    [ContextMenu("Fire")]
    void ChangeLabelName()
    {
        Debug.Log("change name");
        this.gameObject.name = gameObject.name.Replace(" ", "_");
        labelInfo.labelsKey = (LabelsKey)System.Enum.Parse(typeof(LabelsKey), gameObject.name);
    }

    void OnLabelInfo()
    {
        LabelsHandler.Instance.ShowLabel("" + labelInfo.labelsKey);
    }
}