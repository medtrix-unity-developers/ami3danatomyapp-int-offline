﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{
    [SerializeField]
    private const string _consumerSecret = "9D1FB456365DE716A8080786AA3CEF4C1079200CCD7723F8A7E2D0D23764387E";
    [SerializeField]
    private const string _clientId = "3MVG96mGXeuuwTZibJF8Ab1857XMzs3wJ0MohO6ypMslpWYcwpxUNXqz8s4Vl.uoORsw8mW4Az3bjbxVnvo41";
    [SerializeField]
    private const string _callbackURL = "sfdc://mobileapp/done"; //  "https://www.getpostman.com/oauth2/callback";
    [SerializeField]
    private const string _baseGetUrl = "https://amioeu-allergancommunityeu.cs84.force.com/Spark/services/oauth2/authorize";

    private const string SIGNATURE_PARAM_NAME = "signature=";
    private const string ACCESSTOKEN_PARAM_NAME = "access_token=";
    private const string ACCESS_DENIED = "error=";

    //private Text StatusTxt;
    UniWebView webView;
    private GameObject webViewGameObject;

    public Text txtError;
    public Text txtWelcome;
    public GameObject Panel_Login, PanelWelcome;

    // Start is called before the first frame update
    void Start()
    {
        // Create a game object to hold UniWebView and add component.
        webViewGameObject = new GameObject("UniWebView");
        webView = webViewGameObject.AddComponent<UniWebView>();
        webView.Frame = new Rect(0, 0, Screen.width, Screen.height);
        //webView.SetShowToolbar(true);

        webView.OnPageFinished += (view, statusCode, url) => {
            //ParseResponseURL(view, statusCode, url);
            ParseAMIResponseURL(view, statusCode, url);
        };

        //webView.OnShouldClose += (view) => {
        //    Debug.Log(" On Should Close..." + ((UniWebView)view).Url);
        //webView = null;
        //return true;
        //};

        webView.AddUrlScheme("sfdc");
        webView.OnMessageReceived += (view, message) => {
            //print(message.RawMessage);
            Debug.Log(" OnMessageReceived "+ message.RawMessage);
        };
     
    }


    public void OnLoginClicked()
    {

        //we don't have a refresh token so we gotta go through the whole auth process.
        // var url =
        //      "https://amioeu-allergancommunityeu.cs84.force.com/Spark/services/oauth2/authorize?response_type=code&client_id=" + _clientId + "&redirect_uri=" +
        //         CallBackUrl+"&scope=web%20api";
        // "&scope=activity%20nutrition%20heartrate%20location%20profile%20sleep%20weight%20social";

        //Spark User OAuth URL:
        //webView.Load("https://amioeu-allergancommunityeu.cs84.force.com/Spark/services/oauth2/authorize?client_id=3MVG96mGXeuuwTZibJF8Ab1857XMzs3wJ0MohO6ypMslpWYcwpxUNXqz8s4Vl.uoORsw8mW4Az3bjbxVnvo41&response_type=token&redirect_uri=sfdc://mobileapp/done");

        // AMI Portal Oauth End URL:
        webView.Load("https://amioeu-allergancommunityeu.cs84.force.com/AMI/services/oauth2/authorize?response_type=token&client_id=3MVG96mGXeuuwTZibJF8Ab1857XMzs3wJ0MohO6ypMslpWYcwpxUNXqz8s4Vl.uoORsw8mW4Az3bjbxVnvo41&redirect_uri=sfdc://mobileapp/done&state=mystate");
        webView.Show();
     }


    private void ParseResponseURL(UniWebView view, int statusCode, string url)
    {
        Debug.Log(" Page load finished" + view + " STaTCode: " + statusCode + " URL:  " + url);

        Uri myUrl = new Uri(url);
        String queryString =  myUrl.Fragment;
        String queryErrorString = myUrl.Query;

        int i = queryString.IndexOf(ACCESSTOKEN_PARAM_NAME, StringComparison.Ordinal);
        int j = queryErrorString.IndexOf(ACCESS_DENIED, StringComparison.Ordinal);
        

        if (i == -1)
        {
            Debug.Log("Siva is Not authorized...");
            //StatusTxt.text = "Please Try Again.";
        }
        else
        {
            Debug.Log("Siva is authorized...");
            webView.InternalOnShouldClose();
            PlayerPrefs.SetInt("IsUserLoggedAlready", 1);
            PanelWelcome.SetActive(true);
            Panel_Login.SetActive(false);

        }
 
        if(j != -1)
        {
            webView.InternalOnShouldClose();
        }

        //byte[] queryParamContentData = Encoding.UTF8.GetBytes(queryString.Substring(0, i - 1));
        // i - 1 instead of i because of & in the query string 
    }

    private void ParseAMIResponseURL(UniWebView view, int statusCode, string url)
    {
        Debug.Log(" Page load finished" + view + " STaTCode: " + statusCode + " URL:  " + url);
        if (url == "https://amioeu-allergancommunityeu.cs84.force.com/AMI/apex/AGN_AMI_Home_Page")
        {
            Debug.Log("Siva is authorized...");
            Invoke("CloseWebBrowser", 2f);

        }
        else
        {
            //StatusTxt.text = "Please Try Again.";
            Debug.Log("Siva is Not authorized...");
        }
    }
     

    private void CloseWebBrowser()
    {
        webView.InternalOnShouldClose();
        PlayerPrefs.SetInt("IsUserLoggedAlready", 1);
        PanelWelcome.SetActive(true);
        Panel_Login.SetActive(false);
    }

    private string CallBackUrl
    {
        get
        {
            //determine which platform we're running on and use the appropriate url
            if (Application.platform == RuntimePlatform.WindowsEditor)
                return WWW.EscapeURL(_callbackURL);
            else if (Application.platform == RuntimePlatform.Android)
            {
                return WWW.EscapeURL(_callbackURL);
            }
            else
            {
                return WWW.EscapeURL(_callbackURL);
            }
        }
    }

    void DisableError()
    {
        txtError.gameObject.SetActive(false);//.text = "";
    }

    public void BtnWelcome()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Label");
    }


    
}
