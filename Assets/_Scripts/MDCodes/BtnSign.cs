﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BtnSign : MonoBehaviour
{
    public Text buttonText;
    // Image buttonImage;
    public string strAttribute, strSign;
    bool isOn = false;

    private void Start()
    {
        this.GetComponent<Button>().onClick.AddListener(BtnClick);
    }

    public void Init(string attribute, string sign)
    {
        buttonText.text = sign;
        strAttribute = attribute;
        strSign = sign;
    }

    public void BtnClick()
    {    
        if (!isOn)
        {
            isOn = true;

            MDCodeManager.Instance.ButtonState(true, false, this.gameObject);
        }
        else
        {
            isOn = false;

            MDCodeManager.Instance.ButtonState(false, false, this.gameObject);
        }

        InjectionTechniquesHandler.Instance.BtnSign(strAttribute, strSign, isOn, true);
        InjectionTechniquesHandler.Instance.EnableCode(strAttribute);
    }

    public void EnableCode()
    {
        if (isOn)
            InjectionTechniquesHandler.Instance.BtnSign(strAttribute, strSign, isOn, false);
    }

    public void ResetButton()
    {
        isOn = false;
        MDCodeManager.Instance.ButtonState(false, false, this.gameObject);
    }

    public void DisableCode()
    {
        isOn = false;
        InjectionTechniquesHandler.Instance.BtnSign(strAttribute, strSign, false, false);
    }
}