﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MDCode_Cell : MonoBehaviour
{
    //public Text txtMDCode, txtProduct, txtTargetLayer,txtFacialLandmarks, txtTool, txtDelivery, txtVolume;
    public Image image;
    public TextMeshProUGUI textLevel;
    /*
    public void Init(MDCodeInfo info)
    {
        if (info.code.Contains("_"))
        {
            txtMDCode.text = info.code.Substring(0, info.code.IndexOf("_", System.StringComparison.Ordinal));
        }
        else
            txtMDCode.text = info.code;

        txtProduct.text = info.product;
        txtTargetLayer.text = info.target_layer;
        txtFacialLandmarks.text = info.facial_landmarks;              
        txtTool.text = info.tool;
        txtDelivery.text = info.delivery;
        txtVolume.text = info.volume_range;
    }
    */

    public void Init(Sprite flashCard)
    {
        image.sprite = flashCard;
    }

    public void Init(string layerName)
    {
        textLevel.text = layerName;
    }
}