﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MDCode : MonoBehaviour
{
    public int toLayer;
    public Transform to;
    Transform from;

    public LineRenderer lineRenderer;
    
    void Start()
    {
        return;
        from = this.transform;

        if (this.GetComponent<LineRenderer>() != null && !this.transform.parent.name.Equals("Right"))
        {
            switch (this.transform.parent.name)
            {
                case "Left":
                    to = MDCodeManager.Instance.mdCodeLayer[toLayer].MDCode_Left.transform.Find(this.name).transform;
                    break;

                case "Right":
                    to = MDCodeManager.Instance.mdCodeLayer[toLayer].MDCode_Right.transform.Find(this.name).transform;
                    break;

                case "Center":
                    to = MDCodeManager.Instance.mdCodeLayer[toLayer].MDCode_Center.transform.Find(this.name).transform;
                    break;
            }

           
            lineRenderer = this.GetComponent<LineRenderer>();
            lineRenderer.startWidth = 0.001f;
            lineRenderer.endWidth = 0.01f;

            Vector3[] pos = { from.position, to.position };

            //lineRenderer.SetPositions(pos);

            MDCodeManager.Instance.slider.onValueChanged.AddListener(OnChange);
        }
    }

    private void OnChange(float arg0)
    {
        Vector3[] pos = { from.position, to.position };

        lineRenderer.SetPositions(pos);
    }
}