﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BtnAttribute : MonoBehaviour
{
    public Text buttonText;
    public Image buttonImage;
}
