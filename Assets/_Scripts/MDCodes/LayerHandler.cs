﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LayerHandler : MonoBehaviour
{
    [System.Serializable]
    public class MDCodesList
    {
        public List<MDCodeObj> mdCodes_Left = new List<MDCodeObj>();
        public List<MDCodeObj> mdCodes_Right = new List<MDCodeObj>();
        public List<MDCodeObj> mdCodes_Center = new List<MDCodeObj>();
    }

    [System.Serializable]
    public class MDCodeObj
    {
        public string strCode;
        public GameObject objCode;

        public MDCodeObj(string code, GameObject obj)
        {
            strCode = code;
            objCode = obj;
        }
    }

    public GameObject MDCode_Left, MDCode_Right, MDCode_Center;
    public EmotionalAttributes emotionalAttributes = new EmotionalAttributes();
    public MDCodesList mdCodesList = new MDCodesList();

    public TextAsset textAsset;
    public int layerNumber;

    void Start()
    {
        emotionalAttributes = JsonUtility.FromJson<EmotionalAttributes>(textAsset.text);

        /*
        for (int i = 0; i < emotionalAttributes.attributes.Count; i++)
        {
            for (int j = 0; j < emotionalAttributes.attributes[i].signs.Count; j++)
            {
                if (!string.IsNullOrEmpty(emotionalAttributes.attributes[i].signs[j].strCode))
                {
                    string[] temp = emotionalAttributes.attributes[i].signs[j].strCode.Split(',');
                    emotionalAttributes.attributes[i].signs[j].codeList.AddRange(temp.ToList());
                }
            }
        }

        Debug.Log(this.gameObject.name + "\n" + JsonUtility.ToJson(emotionalAttributes));
        */

        foreach (var obj in MDCode_Right.GetComponentsInChildren<MDCode>())
        {
            mdCodesList.mdCodes_Right.Add(new MDCodeObj(obj.name, obj.gameObject));
            obj.gameObject.SetActive(false);
        }

        foreach (var obj in MDCode_Left.GetComponentsInChildren<MDCode>())
        {
            mdCodesList.mdCodes_Left.Add(new MDCodeObj(obj.name, obj.gameObject));
            obj.gameObject.SetActive(false);
        }

        foreach (var obj in MDCode_Center.GetComponentsInChildren<MDCode>())
        {
            mdCodesList.mdCodes_Center.Add(new MDCodeObj(obj.name, obj.gameObject));
            obj.gameObject.SetActive(false);
        }


    }

    public void DisplayCode(string attributes, string sign, bool isOn, bool updateUI)
    {
        //        Debug.Log("Layer:   "+this.gameObject.name+"    attributes:  " + attributes+ "   sign:   " + sign);
        if (string.IsNullOrEmpty(attributes) || string.IsNullOrEmpty(sign))
            return;

        Sign tempSign = emotionalAttributes.attributes.Find(obj => obj.strAttributes.Equals(attributes)).signs.Find(obj => obj.strSign.Equals(sign));

        if (tempSign == null)
            return;

        for (int i = 0; i < tempSign.codeList.Count; i++)
        {
            UpdateCodeStatus(mdCodesList.mdCodes_Right.Find(obj => obj.strCode.Equals(tempSign.codeList[i])), isOn, true, updateUI);
            UpdateCodeStatus(mdCodesList.mdCodes_Left.Find(obj => obj.strCode.Equals(tempSign.codeList[i])), isOn, false, updateUI);
            UpdateCodeStatus(mdCodesList.mdCodes_Center.Find(obj => obj.strCode.Equals(tempSign.codeList[i])), isOn, true, updateUI);
        }
    }

    void UpdateCodeStatus(MDCodeObj objTemp, bool isOn, bool canAdd, bool updateUI)
    {
        if (objTemp != null)
        {
            objTemp.objCode.SetActive(isOn);

            if (canAdd && updateUI)
                InformationHandler.Instance.UpdateMDCode_UI(layerNumber - 1, objTemp.strCode, isOn);
        }
    }

    public void EnableDisableAllCode(bool value)
    {
        foreach (var obj in mdCodesList.mdCodes_Right)
        {
            if (!obj.strCode.Contains("Gtox") && !obj.strCode.Contains("Otox") && !obj.strCode.Contains("Ftox"))
                obj.objCode.SetActive(value);
        }
        foreach (var obj in mdCodesList.mdCodes_Left)
        {
            if (!obj.strCode.Contains("Gtox") && !obj.strCode.Contains("Otox") && !obj.strCode.Contains("Ftox"))
                obj.objCode.SetActive(value);
        }
        foreach (var obj in mdCodesList.mdCodes_Center)
        {
            if (!obj.strCode.Contains("Gtox") && !obj.strCode.Contains("Otox") && !obj.strCode.Contains("Ftox"))
                obj.objCode.SetActive(value);
        }
    }
}