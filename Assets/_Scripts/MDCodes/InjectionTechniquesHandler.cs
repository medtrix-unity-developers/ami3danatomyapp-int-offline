﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InjectionTechniquesHandler : MonoBehaviour
{
    public static InjectionTechniquesHandler Instance;

    public GameObject panel_Signs;
    public List<BtnSign> btnsSigns = new List<BtnSign>();
    MDCodeManager MDCodeManager;

    private void Awake()
    {
        Instance = this;
    }
    void Start()
    {
        MDCodeManager = FindObjectOfType<MDCodeManager>();
    }

    public void AssignSignButton(int index)
    {
        panel_Signs.SetActive(true);

        for (int i = 0; i < btnsSigns.Count; i++)
        {
            btnsSigns[i].ResetButton();
            btnsSigns[i].gameObject.SetActive(false);
        }

        InformationHandler.Instance.CleareCurrenMDCode();

        for (int i = 0; i < MDCodeManager._EmotionalAttributes.attributes[index].signs.Count; i++)
        {
            btnsSigns[i].gameObject.SetActive(true);
            btnsSigns[i].Init(MDCodeManager._EmotionalAttributes.attributes[index].strAttributes, MDCodeManager._EmotionalAttributes.attributes[index].signs[i].strSign);
            btnsSigns[i].BtnClick();
        }
    }

    public void BtnSign(string attribute, string sign, bool isOn, bool updateUI)
    {
        for (int i = 0; i < MDCodeManager.mdCodeLayer.Count; i++)
        {
            MDCodeManager.mdCodeLayer[i].DisplayCode(attribute, sign, isOn, updateUI);
        }
    }

    public void EnableCode(string attribute)
    {
        Attributes tempAttributes = MDCodeManager._EmotionalAttributes.attributes.Find(obj => obj.strAttributes.Equals(attribute));

        for (int i = 0; i < tempAttributes.signs.Count; i++)
        {
            btnsSigns[i].GetComponent<BtnSign>().EnableCode();
        }
    }
}
