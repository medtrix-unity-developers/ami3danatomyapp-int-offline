{
  "data": [
  
    {
      "key": "welcome_info",
      "description": "The AMI Essentials Anatomy Application is a teaching tool that uses a virtual cadaver model that has been digitally reconstituted, with a high degree of realism, from a cadaver dissection done in an anatomy lab. Experience a 360-degree view of this virtual cadaver model with layers of tissue which can be dissected for a better understanding of the anatomical structures present in each layer such as the skin, muscles, fat pads, nerves, and blood vessels."
    },
    {
      "key": "welcome_user",
      "description": "Welcome User!"
    },
    {
      "key": "continue",
      "description": "Continue"
    },
    {
      "key": "quiz_info",
      "description": "Welcome to the AMI Essentials Anatomy quiz. Test your anatomical knowledge and check your score. You will be presented thirty questions related to identification of the structure indicated on the cadaver or matching the labels with the options provided. There is no time limit and for each correct answer five points will be awarded."
    },
    {
      "key": "video_info",
      "description": "Welcome to the video gallery of the AMI Essentials Anatomy Application. View the cadaver dissection procedure and commentary on the different anatomical structures."
    },
    {
      "key": "score",
      "description": "Score"
    },
    {
      "key": "signin",
      "description": "SIGN IN"
    },
    {
      "key": "enter_username",
      "description": "Enter Username"
    },
    {
      "key": "enter_password",
      "description": "Enter Password"
    },
    {
      "key": "login",
      "description": "Login"
    }
    ,
    {
      "key": "are_you_sure",
      "description": "Do you want to exit the app?"
    },
    {
      "key": "exit_yes",
      "description": "Yes"
    },
    {
      "key": "exit_no",
      "description": "No"
    },
    {
      "key": "submit",
      "description": "Submit"
    }
  ]
}